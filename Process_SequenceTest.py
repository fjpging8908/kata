from unittest import TestCase
from Process_Sequence import  Process_Sequence

class Process_SequenceTest(TestCase):
    '''
    def test_process(self):
        s = Process_Sequence()
        self.assertEquals(s.process(''), [0], 'Iteración 1. Ciclo TDD1 Cadena vacía')
    def test_processIteracion1TDD2(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1'), [1], 'Iteración 1. Ciclo TDD2 Numeros: 1')
    def test_processIteracion1TDD3(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1,2'), [2], 'Iteración 1. Ciclo TDD3 Numeros: 2')
    def test_processIteracion1TDD4(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1,2,3,4,5,6,7'), [7], 'Iteración 1. Ciclo TDD4 Numeros: N')


    def test_processIteracion2TDD1(self):
        s = Process_Sequence()
        self.assertEquals(s.process(''), [0,''], 'Iteración 2. Ciclo TDD1 Cadena vacía')

    def test_processIteracion2TDD2(self):
        s = Process_Sequence()
        self.assertEquals(s.process('2'), [1,2], 'Iteración 2. Ciclo TDD2 Numeros: 1')

    def test_processIteracion1TDD3(self):
        s = Process_Sequence()
        self.assertEquals(s.process('3,4'), [2,3], 'Iteración 2. Ciclo TDD3 Numeros: 2')

    def test_processIteracion2TDD4(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1,2,3,4,5,6,7'), [7,1], 'Iteración 2. Ciclo TDD4 Numeros: N')

    def test_processIteracion3TDD1(self):
        s = Process_Sequence()
        self.assertEquals(s.process(''), [0, '',''], 'Iteración 3. Ciclo TDD1 Cadena vacía')

    def test_processIteracion3TDD2(self):
        s = Process_Sequence()
        self.assertEquals(s.process('5'), [1, 5,5], 'Iteración 3. Ciclo TDD2 Numeros: 1')

    def test_processIteracion3TDD3(self):
        s = Process_Sequence()
        self.assertEquals(s.process('3,4,9'), [2,3,9], 'Iteración 3. Ciclo TDD3 Numeros: 2')

    def test_processIteracion3TDD4(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1,2,3,4,5,6,7,10'), [8, 1,10], 'Iteración 3. Ciclo TDD4 Numeros: N')'''

    def test_processIteracion4TDD1(self):
        s = Process_Sequence()
        self.assertEquals(s.process(''), [0, '', '',''], 'Iteración 4. Ciclo TDD1 Cadena vacía')

    def test_processIteracion4TDD2(self):
        s = Process_Sequence()
        self.assertEquals(s.process('5'), [1, 5, 5,5], 'Iteración 4. Ciclo TDD2 Numeros: 1')

    def test_processIteracion4TDD3(self):
        s = Process_Sequence()
        self.assertEquals(s.process('3,9'), [2, 3, 9,6], 'Iteración 4. Ciclo TDD3 Numeros: 2')

    def test_processIteracion4TDD4(self):
        s = Process_Sequence()
        self.assertEquals(s.process('1,2,3,6'), [4, 1, 6,3], 'Iteración 4. Ciclo TDD4 Numeros: N')
