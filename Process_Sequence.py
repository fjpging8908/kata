class Process_Sequence:
    # Iteration 4; Ciclo TDD4. Devolver un arreglo. número de elementos,Minimo,Maximo,Promedio.  Numeros: N (Verde)
    def process(Self,cadena):
        if cadena=='':
            return [0,'','','']
        elif len(cadena) == 1:
            return [1,int(cadena),int(cadena),int(cadena)]
        elif ',' in cadena:
            Numeros = cadena.split(',')
            Cant = len(Numeros)
            Min = int(min(Numeros))
            Max = int(max(Numeros))
            Sum=0
            for num in  Numeros:
                Sum=Sum + int(num)
            return [Cant, Min,Max,(Sum/Cant)]